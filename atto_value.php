<?php

$array_atto_toolbar_value = array(
		'collapse' => array('collapse'),
		'style1' => array('title', 'bold', 'italic', 'morebackcolors'),
		'fuente' => array('collmorefontcolors', 'fontfamily', 'fontsize', 'stylesapse'),
		'list' => array('unorderedlist', 'orderedlist'),
		'links' => array('link'),
		'files' => array('wordimport', 'image', 'media', 'managefiles'),
		'style3' => array('underline', 'strike', 'subscript', 'superscript', 'emoticon'),
		'align' => array('align', 'htmlplus', 'fullscreen', 'justify'),
		'indent' => array('indent', 'justify'),
		'insert' => array('equation', 'charmap', 'table', 'clear', 'rtl'),
		'undo' => array('undo', 'multilang2', 'corrections', 'count', 'preview'),
		'extras' => array('chemistry', 'easychem', 'pastespecial'),
		'accessibility' => array('accessibilitychecker', 'accessibilityhelper'),
);
